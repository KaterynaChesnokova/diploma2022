﻿using DataAccessLayer;

namespace TempConsole
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var context = new PurchasingContext())
            {
                context.Database.EnsureCreated();
            }
        }
    }
}
