﻿using BusinessLayer.Models.UserAuthenticationModels;
using Microsoft.AspNetCore.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BusinessLayer.ServicesInterfaces
{
    public interface IUserService
    {
        Task<bool> UserCheckPasswordAsync(UserModel user, string password);
        Task<IdentityResult> UserCreateAsync(UserModel user, string password);
        Task<UserModel> UserFindByIdAsync(string id);
        Task<UserModel> UserFindByEmailAsync(string email);
        Task<IList<string>> UserGetRolesAsync(UserModel user);
        Task<bool> RoleExistsAsync(string roleName);
        Task<IdentityResult> RoleCreateAsync(RoleModel role);
        Task<IdentityResult> AddToRoleAsync(UserModel user, string role);
        Task<IdentityResult> CreateAndAddToRole(UserModel user, string password, string role);
        Task<IdentityResult> UserDeleteAccountAsync(string email);
    }
}
