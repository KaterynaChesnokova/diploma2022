﻿using AutoMapper;
using BusinessLayer.Models.UserAuthenticationModels;
using DataAccessLayer.DataEntities.UserEntities;

namespace BusinessLayer
{
    /// <summary>
    /// automapper configuration
    /// </summary>
    public class MapperProfile : Profile
    {
        public MapperProfile()
        {
            CreateMap<UserBase, UserModel>().ReverseMap();
        }
    }
}
