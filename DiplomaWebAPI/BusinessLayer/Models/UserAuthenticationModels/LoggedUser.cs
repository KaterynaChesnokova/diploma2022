﻿using System;
using System.Collections.Generic;

namespace BusinessLayer.Models.UserAuthenticationModels
{
    public class LoggedUser
    {
        public string Id { get; set; }
        public string Email { get; set; }
        public string Token { get; set; }
        public DateTime Expiration { get; set; }

        public IEnumerable<string> Roles { get; set; }
    }
}
