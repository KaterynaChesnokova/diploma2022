﻿using System.ComponentModel.DataAnnotations;

namespace BusinessLayer.Models.UserAuthenticationModels
{
    public class RoleModel
    {
        [Required]
        public string Id { get; set; }

        [Required]
        public string Name { get; set; }

        public string NormalizedName { get; set; }

        public string ConcurrencyStamp { get; set; }
    }
}
