﻿using System;
using System.Runtime.Serialization;

namespace BusinessLayer
{
    /// <summary>
    /// exception that is thrown when action cannot be made as
    /// needed data was not found
    /// </summary>
    [Serializable]
    public class PurchasingNotFoundException : Exception
    {
        public PurchasingNotFoundException() { }

        public PurchasingNotFoundException(string message) : base(message) { }

        public PurchasingNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PurchasingNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
