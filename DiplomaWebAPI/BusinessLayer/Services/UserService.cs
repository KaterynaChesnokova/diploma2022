﻿using System.Collections.Generic;
using System.Threading.Tasks;

using AutoMapper;
using Microsoft.AspNetCore.Identity;

using BusinessLayer.Models.UserAuthenticationModels;
using BusinessLayer.ServicesInterfaces;
using DataAccessLayer.DataEntities.UserEntities;
using DataAccessLayer.Interfaces;

namespace BusinessLayer.Services
{
    public class UserService : IUserService
    {
        readonly IUnitOfWork uow;
        readonly IMapper mapper;

        public UserService(IUnitOfWork _uow, IMapper profile)
        {
            uow = _uow;
            mapper = profile;
        }

        public async Task<UserModel> UserFindByIdAsync(string id)
        {
            return mapper.Map<UserModel>(await uow.UserBaseRepository.UserFindByIdAsync(id));
        }

        public async Task<UserModel> UserFindByEmailAsync(string email)
        {
            return mapper.Map<UserModel>(await uow.UserBaseRepository.UserFindByEmailAsync(email));
        }

        public async Task<bool> UserCheckPasswordAsync(UserModel user, string password)
        {
            return await uow.UserBaseRepository.UserCheckPasswordAsync(mapper.Map<UserBase>(user), password);
        }

        public async Task<IList<string>> UserGetRolesAsync(UserModel user)
        {
            return await uow.UserBaseRepository.UserGetRolesAsync(mapper.Map<UserBase>(user));
        }

        public async Task<IdentityResult> UserCreateAsync(UserModel user, string password)
        {
            return await uow.UserBaseRepository.UserCreateAsync(mapper.Map<UserBase>(user), password);
        }

        public async Task<bool> RoleExistsAsync(string roleName)
        {
            return await uow.UserBaseRepository.RoleExistsAsync(roleName);
        }

        public async Task<IdentityResult> RoleCreateAsync(RoleModel role)
        {
            return await uow.UserBaseRepository.CreateAsync(mapper.Map<IdentityRole>(role));
        }

        /// <summary>
        /// adds new role to user
        /// </summary>
        public async Task<IdentityResult> AddToRoleAsync(UserModel user, string role)
        {
            return await uow.UserBaseRepository.AddToRoleAsync(mapper.Map<UserBase>(user), role);
        }

        /// <summary>
        /// creates new user and adds him to role
        /// </summary>
        public async Task<IdentityResult> CreateAndAddToRole(UserModel user, string password, string role)
        {
            return await uow.UserBaseRepository.CreateAndAddToRole(mapper.Map<UserBase>(user), password, role);
        }

        public async Task<IdentityResult> UserDeleteAccountAsync(string email)
        {
            var user = await uow.UserBaseRepository.UserFindByEmailAsync(email);
            if (user == null)
                throw new PurchasingNotFoundException("User is not defined.");

            await uow.SaveAsync();
            return await uow.UserBaseRepository.UserDeleteAccountAsync(email);
        }
    }
}
