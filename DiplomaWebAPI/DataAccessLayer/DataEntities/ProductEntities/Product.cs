﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using DataAccessLayer.DataEntities.OrderEntities;

namespace DataAccessLayer.DataEntities.ProductEntities
{
    public class Product : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        [NotMapped]
        public ICollection<LimitProduct> LimitProducts { get; set; }
        [NotMapped]
        public ICollection<ProductSpecification> ProductSpecifications { get; set; }
        public ICollection<OrderItem> OrderItems { get; set; }
        public ICollection<Limit> Limits { get; set; }
    }
}
