﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.ProductEntities
{
    public class LimitProduct
    {
        public int LimitId { get; set; }
        [ForeignKey("LimitId")]
        public virtual Limit Limit { get; set; }


        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }
    }
}
