﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.ProductEntities
{
    public class SpecValue : BaseEntity
    {
        public int SpecificationId { get; set; }
        [ForeignKey("SpecificationId")]
        public Specification Specification { get; set; }

        [Required]
        public string Value { get; set; }
    }
}
