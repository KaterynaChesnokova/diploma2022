﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using DataAccessLayer.DataEntities.UserEntities;

namespace DataAccessLayer.DataEntities.ProductEntities
{
    public class Limit : BaseEntity
    {
        [Required]
        public DateTime StartDate { get; set; }
        [Required]
        public DateTime EndDate { get; set; }

        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public Employee Employee { get; set; }

        [NotMapped]
        public ICollection<LimitProduct> LimitProducts { get; set; }
    }
}
