﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.ProductEntities
{
    public class Specification : BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public SpecValue SpecValue { get; set; }

        [NotMapped]
        public ICollection<ProductSpecification> ProductSpecifications { get; set; }
    }
}
