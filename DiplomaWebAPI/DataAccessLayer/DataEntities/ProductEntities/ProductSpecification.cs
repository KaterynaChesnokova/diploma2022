﻿using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.ProductEntities
{
    public class ProductSpecification
    {
        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public virtual Product Product { get; set; }


        public int SpecificationId { get; set; }
        [ForeignKey("SpecificationId")]
        public virtual Specification Specification { get; set; }
    }
}
