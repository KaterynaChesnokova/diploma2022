﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.DataEntities.AddressEntities
{
    public class Region : BaseEntity
    {
        [Required]
        public string RegionName { get; set; }

        public ICollection<RegionDistrict> Districts { get; set; }
    }
}
