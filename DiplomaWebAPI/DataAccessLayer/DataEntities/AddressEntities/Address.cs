﻿using DataAccessLayer.DataEntities.OrderEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.AddressEntities
{
    public class Address : BaseEntity
    {
        [Required]
        public string Country { get; set; }
        
        public int RegionDistrictId { get; set; }
        [ForeignKey("RegionDistrictId")]
        public RegionDistrict RegionDistrict { get; set; }

        [Required]
        public string City { get; set; }
        [Required]
        public string Street { get; set; }
        [Required]
        public string BuildingNumber { get; set; }



        //public int OfficeId { get; set; }
        //[ForeignKey("OfficeId")]
        //public Office Office { get; set; }

        //public int WarehouseId { get; set; }
        //[ForeignKey("WarehouseId")]
        //public Warehouse Warehouse { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
