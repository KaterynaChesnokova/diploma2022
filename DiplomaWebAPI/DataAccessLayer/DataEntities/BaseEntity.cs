﻿using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.DataEntities
{
    public class BaseEntity
    {
        [Required]
        public int Id { get; set; }
    }
}
