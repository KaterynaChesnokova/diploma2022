﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

using DataAccessLayer.DataEntities.OrderEntities;
using DataAccessLayer.DataEntities.UserEntities;

namespace DataAccessLayer.DataEntities
{
    public class EmployeeOrder
    {
        public int EmployeeId { get; set; }
        [ForeignKey("EmployeeId")]
        public virtual Employee Employee { get; set; }


        public int OrderId { get; set; }
        [ForeignKey("OrderId")]
        public virtual Order Order { get; set; }

        [Required]
        public DateTime AssignedTime { get; set; }
        [Required]
        public bool isAssigned { get; set; }
    }
}
