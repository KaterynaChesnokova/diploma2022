﻿using DataAccessLayer.DataEntities.AddressEntities;
using DataAccessLayer.DataEntities.UserEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.OrderEntities
{
    public class Order : BaseEntity
    {
        public int CustomerId { get; set; }
        [ForeignKey("CustomerId")]
        public Customer Customer { get; set; }

        public int OrderStatusId { get; set; }
        [ForeignKey("OrderStatusId")]
        public OrderStatus Status { get; set; }

        public int WarehouseId { get; set; }
        [ForeignKey("WarehouseId")]
        public Warehouse Warehouse { get; set; }

        public int DealAddressId { get; set; }
        [ForeignKey("DealAddressId")]
        public Address DealAddress { get; set; }

        [NotMapped]
        public ICollection<EmployeeOrder> EmployeeOrders { get; set; }

        public ICollection<OrderItem> Items { get; set; }
    }
}
