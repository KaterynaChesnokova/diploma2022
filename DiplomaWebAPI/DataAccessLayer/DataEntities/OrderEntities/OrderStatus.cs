﻿using System.Collections.Generic;

namespace DataAccessLayer.DataEntities.OrderEntities
{
    public class OrderStatus : BaseEntity
    {
        public string Name { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
