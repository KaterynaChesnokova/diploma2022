﻿using DataAccessLayer.DataEntities.ProductEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.OrderEntities
{
    public class OrderItem : BaseEntity
    {
        [Required]
        public int Count { get; set; }
        [Required]
        public decimal UnitPrice { get; set; }

        public int ProductId { get; set; }
        [ForeignKey("ProductId")]
        public Product Product { get; set; }

        public int MeasureId { get; set; }
        [ForeignKey("MeasureId")]
        public Measure Measure { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
