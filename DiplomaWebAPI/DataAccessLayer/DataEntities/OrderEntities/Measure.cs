﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace DataAccessLayer.DataEntities.OrderEntities
{
   public class Measure:BaseEntity
    {
        [Required]
        public string Name { get; set; }

        public ICollection<OrderItem> OrderItems { get; set; }
    }
}
