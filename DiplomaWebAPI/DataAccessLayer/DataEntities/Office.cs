﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using DataAccessLayer.DataEntities.AddressEntities;
using DataAccessLayer.DataEntities.UserEntities;

namespace DataAccessLayer.DataEntities
{
    public class Office : BaseEntity
    {
        public int OfficeAddressId { get; set; }
        [ForeignKey("OfficeAddressId")]
        public Address OfficeAddress { get; set; }

        public ICollection<Employee> Employees { get; set; }
    }
}
