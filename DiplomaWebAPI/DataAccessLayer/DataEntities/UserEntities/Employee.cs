﻿using DataAccessLayer.DataEntities.OrderEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.UserEntities
{
    public class Employee : BaseEntity
    {
        public string UserBaseId { get; set; }
        [ForeignKey("UserBaseId")]
        public UserBase UserBase { get; set; }

        public int OfficeId { get; set; }
        [ForeignKey("OfficeId")]
        public Office EmployeeOffice { get; set; }

        [NotMapped]
        public ICollection<EmployeeOrder> EmployeeOrders { get; set; }
    }
}
