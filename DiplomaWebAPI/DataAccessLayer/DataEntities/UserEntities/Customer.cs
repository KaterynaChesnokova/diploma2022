﻿using DataAccessLayer.DataEntities.OrderEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities.UserEntities
{
    public class Customer : BaseEntity
    {
        public string UserBaseId { get; set; }
        [ForeignKey("UserBaseId")]
        public UserBase UserBase { get; set; }

        [Required]
        public string RegistrationNumber { get; set; }
        [Required]
        public string PassportSeries { get; set; }
        [Required]
        public string PassportNumber { get; set; }
        [Required]
        public byte[] OwnershipCertificate { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
