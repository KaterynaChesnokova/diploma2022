﻿using DataAccessLayer.DataEntities.AddressEntities;
using DataAccessLayer.DataEntities.OrderEntities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace DataAccessLayer.DataEntities
{
    public class Warehouse : BaseEntity
    {
        public int WarehouseAddressId { get; set; }
        [ForeignKey("WarehouseAddressId")]
        public Address WarehouseAddress { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
