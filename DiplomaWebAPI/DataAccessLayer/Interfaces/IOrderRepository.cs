﻿using System.Linq;
using System.Threading.Tasks;

using DataAccessLayer.DataEntities.OrderEntities;

namespace DataAccessLayer.Interfaces
{
    public interface IOrderRepository : IRepository<Order>
    {
        IQueryable<Order> GetAllWithDetails();

        Task<Order> GetByIdWithDetailsAsync(int id);
    }
}
