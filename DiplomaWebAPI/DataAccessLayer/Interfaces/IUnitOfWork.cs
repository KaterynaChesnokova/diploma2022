﻿using System.Threading.Tasks;

namespace DataAccessLayer.Interfaces
{
    public interface IUnitOfWork
    {
        IUserBaseRepository UserBaseRepository { get; }
        IEmployeeRepository EmployeesRepository { get; }
        ICustomerRepository CustomerRepository { get; }
        IOrderRepository OrderRepository { get; }
        IProductRepository ProductRepository { get; }

        Task<int> SaveAsync();
    }
}
