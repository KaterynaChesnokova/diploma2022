﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using DataAccessLayer.DataEntities.UserEntities;

namespace DataAccessLayer.Interfaces
{
    public interface IUserBaseRepository
    {
        Task<bool> UserCheckPasswordAsync(UserBase user, string password);
        Task<IdentityResult> UserCreateAsync(UserBase user, string password);
        Task<UserBase> UserFindByEmailAsync(string email);
        Task<IList<string>> UserGetRolesAsync(UserBase user);
        Task<bool> RoleExistsAsync(string roleName);
        Task<IdentityResult> CreateAsync(IdentityRole role);
        Task<IdentityResult> AddToRoleAsync(UserBase user, string role);
        Task<IdentityResult> CreateAndAddToRole(UserBase user, string password, string role);
        Task<IdentityResult> UserDeleteAccountAsync(string userName);
        Task<UserBase> UserFindByIdAsync(string id);
    }
}
