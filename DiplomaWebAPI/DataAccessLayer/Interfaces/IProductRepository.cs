﻿using System.Linq;
using System.Threading.Tasks;

using DataAccessLayer.DataEntities.ProductEntities;

namespace DataAccessLayer.Interfaces
{
    public interface IProductRepository : IRepository<Product>
    {
        IQueryable<Product> GetAllWithDetails();

        Task<Product> GetByIdWithDetailsAsync(int id);
    }
}
