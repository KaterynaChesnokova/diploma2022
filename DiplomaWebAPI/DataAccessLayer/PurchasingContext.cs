﻿using DataAccessLayer.DataEntities;
using DataAccessLayer.DataEntities.AddressEntities;
using DataAccessLayer.DataEntities.OrderEntities;
using DataAccessLayer.DataEntities.ProductEntities;
using DataAccessLayer.DataEntities.UserEntities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace DataAccessLayer
{
    public class PurchasingContext : IdentityDbContext<UserBase>
    {
        public DbSet<Address> Addresses { get; set; }
        public DbSet<Region> Regions { get; set; }
        public DbSet<RegionDistrict> RegionDistricts { get; set; }
        public DbSet<Measure> Measures { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItems { get; set; }
        public DbSet<OrderStatus> OrderStatuses { get; set; }
        public DbSet<Limit> Limits { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Specification> Specifications { get; set; }
        public DbSet<SpecValue> SpecValues { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<Employee> Employees { get; set; }
        public DbSet<UserBase> UserBases { get; set; }
        public DbSet<Office> Offices { get; set; }
        public DbSet<Warehouse> Warehouses { get; set; }

        public PurchasingContext()
        {
        }

        public PurchasingContext(DbContextOptions<PurchasingContext> options)
            : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Server=(localdb)\\mssqllocaldb;Initial Catalog=Purchasing;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            foreach (var relationship in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                relationship.DeleteBehavior = DeleteBehavior.Restrict;
            }

            modelBuilder.Entity<ProductSpecification>()
                .HasKey(s => new { s.ProductId, s.SpecificationId });

            modelBuilder.Entity<EmployeeOrder>()
                                .HasKey(s => new { s.EmployeeId, s.OrderId });

            modelBuilder.Entity<LimitProduct>()
                                .HasKey(s => new { s.LimitId, s.ProductId });

            //modelBuilder.Entity<Genre>()
            //    .HasIndex(u => u.Name)
            //    .IsUnique();

            InitRolesAndUsers(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }

        private void InitRolesAndUsers(ModelBuilder modelBuilder)
        {
            var role1 = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Superadmin", NormalizedName = "SUPERADMIN" };
            var role2 = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Admin", NormalizedName = "ADMIN" };
            var role3 = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "CustomerRelations", NormalizedName = "CUSTOMERRELATIONS" };
            var role4 = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Inspector", NormalizedName = "INSPECTOR" };
            var role5 = new IdentityRole { Id = Guid.NewGuid().ToString(), Name = "Customer", NormalizedName = "CUSTOMER" };

            modelBuilder.Entity<IdentityRole>().HasData(role1, role2, role3, role4, role5);

            var superadminBase = new UserBase
            {
                FirstName = "Superadmin",
                LastName = "1",
                IsDeleted = false,
                Id = Guid.NewGuid().ToString(),
                Email = "superadmin@gmail.com",
                NormalizedEmail = "SUPERADMIN@GMAIL.COM"
            };

            var adminBase = new UserBase
            {
                FirstName = "Admin",
                LastName = "1",
                IsDeleted = false,
                Id = Guid.NewGuid().ToString(),
                Email = "admin@gmail.com",
                NormalizedEmail = "ADMIN@GMAIL.COM"
            };

            var customerRelationsBase = new UserBase
            {
                FirstName = "CustomerRelatons",
                LastName = "1",
                IsDeleted = false,
                Id = Guid.NewGuid().ToString(),
                Email = "cr1@gmail.com",
                NormalizedEmail = "CR1@GMAIL.COM"
            };

            var inspectorBase = new UserBase
            {
                FirstName = "Inspector",
                LastName = "1",
                IsDeleted = false,
                Id = Guid.NewGuid().ToString(),
                Email = "i1@gmail.com",
                NormalizedEmail = "I1@GMAIL.COM"
            };

            var customerBase = new UserBase
            {
                FirstName = "Customer",
                LastName = "1",
                IsDeleted = false,
                Id = Guid.NewGuid().ToString(),
                Email = "c1@gmail.com",
                NormalizedEmail = "C1@GMAIL.COM"
            };

            var hasher = new PasswordHasher<UserBase>();
            var superadminPassword = hasher.HashPassword(superadminBase, "Superadmin1!");
            var adminPassword = hasher.HashPassword(adminBase, "Admin1!");
            var customerRelatonsPassword = hasher.HashPassword(customerRelationsBase, "cr1!");
            var inspectorPassword = hasher.HashPassword(inspectorBase, "i1!");
            var customerPassword = hasher.HashPassword(customerBase, "c1!");

            superadminBase.PasswordHash = superadminPassword;
            adminBase.PasswordHash = adminPassword;
            customerRelationsBase.PasswordHash = customerRelatonsPassword;
            inspectorBase.PasswordHash = inspectorPassword;
            customerBase.PasswordHash = customerPassword;

            modelBuilder.Entity<IdentityUserRole<string>>().HasData(
                new IdentityUserRole<string> { RoleId = role1.Id, UserId = superadminBase.Id },
                new IdentityUserRole<string> { RoleId = role2.Id, UserId = adminBase.Id },
                new IdentityUserRole<string> { RoleId = role3.Id, UserId = customerRelationsBase.Id },
                new IdentityUserRole<string> { RoleId = role4.Id, UserId = inspectorBase.Id },
                new IdentityUserRole<string> { RoleId = role5.Id, UserId = customerBase.Id });

            modelBuilder.Entity<UserBase>().HasData(superadminBase, adminBase, customerRelationsBase, inspectorBase, customerBase);

            //add employee(admins) and customer
        }
    }
}
