﻿using System.Threading.Tasks;

using DataAccessLayer.Interfaces;
using DataAccessLayer.Repositories;

namespace DataAccessLayer
{
    public class UnitOfWork : IUnitOfWork
    {
        readonly PurchasingContext _db;

        public UnitOfWork(PurchasingContext db, IUserBaseRepository userBaseRepository, IEmployeeRepository employeesRepository,
            ICustomerRepository customerRepository, IOrderRepository orderRepository, IProductRepository productRepository)
        {
            _db = db;
            UserBaseRepository = userBaseRepository;
            EmployeesRepository = employeesRepository;
            CustomerRepository = customerRepository;
            OrderRepository = orderRepository;
            ProductRepository = productRepository;
        }

        public IUserBaseRepository UserBaseRepository { get; }

        public IEmployeeRepository EmployeesRepository { get; }

        public ICustomerRepository CustomerRepository { get; }

        public IOrderRepository OrderRepository { get; }

        public IProductRepository ProductRepository { get; }

        public async Task<int> SaveAsync()
        {
            return await _db.SaveChangesAsync();
        }
    }
}
