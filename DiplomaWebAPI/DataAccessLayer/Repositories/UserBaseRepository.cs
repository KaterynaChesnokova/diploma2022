﻿using DataAccessLayer.DataEntities.UserEntities;
using DataAccessLayer.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DataAccessLayer.Repositories
{
    public class UserBaseRepository : IUserBaseRepository
    {
        readonly UserManager<UserBase> _userManager;
        readonly RoleManager<IdentityRole> _roleManager;
        readonly PurchasingContext _db;

        public UserBaseRepository(PurchasingContext db, UserManager<UserBase> userManager, RoleManager<IdentityRole> roleManager)
        {
            _userManager = userManager;
            _roleManager = roleManager;
            _db = db;
        }

        public async Task<UserBase> UserFindByIdAsync(string id)
        {
            return await _db.UserBases.FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task<UserBase> UserFindByEmailAsync(string email)
        {
            return await _db.UserBases.FirstOrDefaultAsync(x => x.Email == email);
        }

        public async Task<bool> UserCheckPasswordAsync(UserBase user, string password)
        {
            return await _userManager.CheckPasswordAsync(user, password);
        }

        public async Task<IList<string>> UserGetRolesAsync(UserBase user)
        {
            return await _userManager.GetRolesAsync(user);
        }

        public async Task<IdentityResult> UserCreateAsync(UserBase user, string password)
        {
            return await _userManager.CreateAsync(user, password);
        }

        public async Task<bool> RoleExistsAsync(string roleName)
        {
            return await _roleManager.RoleExistsAsync(roleName);
        }

        public async Task<IdentityResult> CreateAsync(IdentityRole role)
        {
            return await _roleManager.CreateAsync(role);
        }

        public async Task<IdentityResult> AddToRoleAsync(UserBase user, string role)
        {
            return await _userManager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> CreateAndAddToRole(UserBase user, string password, string role)
        {
            await _userManager.CreateAsync(user, password);
            return await _userManager.AddToRoleAsync(user, role);
        }

        public async Task<IdentityResult> UserDeleteAccountAsync(string email)
        {
            var user = await UserFindByEmailAsync(email);
            return await _userManager.DeleteAsync(user);
        }
    }
}
