﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

using DataAccessLayer.DataEntities.ProductEntities;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Repositories
{
    public class ProductRepository : IProductRepository
    {
        private readonly PurchasingContext _db;

        public ProductRepository(PurchasingContext db)
        {
            _db = db;
        }

        public async Task AddAsync(Product entity)
        {
            await _db.Products.AddAsync(entity);
        }

        public void Delete(Product entity)
        {
            _db.Products.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _db.Products.FindAsync(id);
            _db.Products.Remove(entity);
        }

        public IQueryable<Product> FindAll()
        {
            return _db.Products;
        }

        public IQueryable<Product> GetAllWithDetails()
        {
            return _db.Products.Include(x => x.OrderItems)
                                .Include(x => x.ProductSpecifications).ThenInclude(x => x.Specification).ThenInclude(x => x.SpecValue);
        }

        public async Task<Product> GetByIdAsync(int id)
        {
            return await _db.Products.FindAsync(id);
        }

        public async Task<Product> GetByIdWithDetailsAsync(int id)
        {
            return await GetAllWithDetails().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Product entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }
    }
}
