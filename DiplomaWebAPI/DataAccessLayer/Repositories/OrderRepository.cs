﻿using Microsoft.EntityFrameworkCore;
using System.Linq;
using System.Threading.Tasks;

using DataAccessLayer.DataEntities.OrderEntities;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Repositories
{
    public class OrderRepository : IOrderRepository
    {
        private readonly PurchasingContext _db;

        public OrderRepository(PurchasingContext db)
        {
            _db = db;
        }

        public async Task AddAsync(Order entity)
        {
            await _db.Orders.AddAsync(entity);
        }

        public void Delete(Order entity)
        {
            _db.Orders.Remove(entity);
        }

        public async Task DeleteByIdAsync(int id)
        {
            var entity = await _db.Orders.FindAsync(id);
            _db.Orders.Remove(entity);
        }

        public IQueryable<Order> FindAll()
        {
            return _db.Orders;
        }

        public IQueryable<Order> GetAllWithDetails()
        {
            return _db.Orders.Include(x => x.Customer).ThenInclude(x => x.UserBase)
                            .Include(x => x.EmployeeOrders)
                            .Include(x => x.DealAddress).ThenInclude(x => x.RegionDistrict)
                            .Include(x => x.Status)
                            .Include(x => x.Items)
                            .Include(x => x.Warehouse).ThenInclude(x => x.WarehouseAddress).ThenInclude(x => x.RegionDistrict);
        }

        public async Task<Order> GetByIdAsync(int id)
        {
            return await _db.Orders.FindAsync(id);
        }

        public async Task<Order> GetByIdWithDetailsAsync(int id)
        {
            return await GetAllWithDetails().FirstOrDefaultAsync(x => x.Id == id);
        }

        public void Update(Order entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
        }
    }
}
