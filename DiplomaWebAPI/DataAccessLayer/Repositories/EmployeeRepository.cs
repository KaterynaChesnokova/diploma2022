﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;

using DataAccessLayer.DataEntities.UserEntities;
using DataAccessLayer.Interfaces;

namespace DataAccessLayer.Repositories
{
    public class EmployeeRepository : IEmployeeRepository
    {
        readonly PurchasingContext _db;

        public EmployeeRepository(PurchasingContext db)
        {
            _db = db;
        }

        //public async Task<Employee> UserFindByIdAsync(string id)
        //{
        //    return await _db.Employees.FirstOrDefaultAsync(x => x.UserBase.Id == id);
        //}

        //public async Task<Employee> UserFindByEmailAsync(string email)
        //{
        //    return await _db.Employees.FirstOrDefaultAsync(x => x.UserBase.Email == email);
        //}

        //public async Task<bool> UserCheckPasswordAsync(Employee user, string password)
        //{
        //    return await _userManager.CheckPasswordAsync(user, password);
        //}

        //public async Task<IList<string>> UserGetRolesAsync(Employee user)
        //{
        //    return await _userManager.GetRolesAsync(user);
        //}

        //public async Task<IdentityResult> UserCreateAsync(UserBase user, string password)
        //{
        //    return await _userManager.CreateAsync(new Employee { UserBase = user }, password);
        //}

        //public async Task<bool> RoleExistsAsync(string roleName)
        //{
        //    return await _roleManager.RoleExistsAsync(roleName);
        //}

        //public async Task<IdentityResult> CreateAsync(IdentityRole role)
        //{
        //    return await _roleManager.CreateAsync(role);
        //}

        //public async Task<IdentityResult> AddToRoleAsync(Employee user, string role)
        //{
        //    return await _userManager.AddToRoleAsync(user, role);
        //}

        //public async Task<IdentityResult> CreateAndAddToRole(Employee user, string password, string role)
        //{
        //    await _userManager.CreateAsync(user, password);
        //    return await _userManager.AddToRoleAsync(user, role);
        //}
    }
}
