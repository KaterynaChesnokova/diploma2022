import { Employee } from './../../../interfaces/employee/employee';
import { Role } from '../../../interfaces/role';
import { RoleService } from '../../../services/role-service/role.service';
import { EmployeeService } from '../../../services/employee-service/employee.service';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';
import { User } from '../../../interfaces/user';
import { AuthService } from '../../../services/auth-service/auth.service';

@Component({
  selector: 'app-employee-view',
  templateUrl: './employee-view.component.html',
  styleUrls: ['./employee-view.component.css']
})
export class EmployeeViewComponent implements OnInit {

  roles:string[]=[];
  employeesRoles:Role[]=[];
  employee!:Employee;

  user!:User;
  updateEmployeeForm!:FormGroup;
  error!:string;

  selectedFW!:FormControl;

  constructor(
    private authService: AuthService,
    private employeeService:EmployeeService,
    private roleService:RoleService,
    private route:ActivatedRoute,
    private router:Router
  ) { }

  ngOnInit(): void {
    let currentUser=this.authService.getCurrentUserValue();
    if(currentUser!=null)
      this.roles=currentUser.roles;
    this.error='';

    this.selectedFW = new FormControl();

    let param=this.route.snapshot.paramMap.get('employee');
    if(param==null)
      this.router.navigate(['/employees']);
    else{
      let employeeId=Number(param);
      this.employeeService.getEmployee(employeeId).subscribe((data)=>{
        // if(data==null)
        //     this.noTexts=`No texts with genre ${genre} were found.`;
        // else
          this.employee=data;
      });
    }

    this.roleService.getEmployeeRoles().subscribe((data)=>{
      this.employeesRoles=data;
    });
  }

  updateEmployee():void{

  }
}
