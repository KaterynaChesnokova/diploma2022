import { User } from './../../../interfaces/user';
import { AuthService } from './../../../services/auth-service/auth.service';
import { Component, OnInit } from '@angular/core';
import { Role } from '../../../interfaces/role';
import { RoleService } from '../../../services/role-service/role.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-employee-create',
  templateUrl: './employee-create.component.html',
  styleUrls: ['./employee-create.component.css']
})
export class EmployeeCreateComponent implements OnInit {

  roles:string[]=[];
  employeesRoles:Role[]=[];

  user!:User;
  addEmployeeForm!:FormGroup;
  error!:string;

  selectedFW!:FormControl;

  constructor(
    private authService: AuthService,
    private roleService:RoleService,
    private router:Router
  ) { }

  ngOnInit(): void {
    let currentUser=this.authService.getCurrentUserValue();
    if(currentUser!=null)
      this.roles=currentUser.roles;
    this.error='';

    this.selectedFW = new FormControl();
    this.roleService.getEmployeeRoles().subscribe((data)=>{
      this.employeesRoles=data;
    });
  }

  addEmployee():void{

  }

}
