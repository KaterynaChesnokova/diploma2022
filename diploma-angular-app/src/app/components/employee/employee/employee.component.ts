import { RoleService } from './../../../services/role-service/role.service';
import { Role } from './../../../interfaces/role';
import { ViewEmployee } from './../../../interfaces/employee/view-employee';
import { Employee } from 'src/app/interfaces/employee/employee';
import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { User } from 'src/app/interfaces/user';
import { AuthService } from '../../../services/auth-service/auth.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css']
})
export class EmployeesComponent implements OnInit {

  roles:string[]=[];
  selectedFW!:FormControl;

  employees:ViewEmployee[]=[
    {id: '1', first_name:'Mykola', last_name: 'Akymenko', email: 'employee1@gmail.com', role: 'Admin', isDeleted: false},
    {id: '2', first_name:'Valeriya', last_name: 'Barilko', email: 'employee2@gmail.com', role: 'CustomerRelations', isDeleted: true},
  ];

  rolesList:Role[]=[
    {name: 'Superadmin'},
    {name: 'Admin'},
    {name: 'CustomerRelations'},
    {name: 'Inspector'}
  ];

  form!:FormGroup;
  error!:string;

  constructor(
    private authService: AuthService,
    private roleService: RoleService,
    private router:Router
    ) { }

  ngOnInit(): void {
    let currentUser=this.authService.getCurrentUserValue();
    if(currentUser!=null)
      this.roles=currentUser.roles;
    this.error='';
    
    // this.roleService.getEmployeeRoles().subscribe((data)=>{
    //     this.rolesList=data;
    //   });

    this.selectedFW = new FormControl();
    this.form=new FormGroup({
      name:new FormControl(''),
      surname:new FormControl('')
    });
  }

  onSaveChanges(userId:string){
    
  }

  onDelete(user:ViewEmployee){
      if(confirm(`Are you sure you want to delete ${user.email}?`)) {
      this.authService.deleteAccount(user.id).subscribe((x)=>{
        // this.authorService.getAllAuthors().subscribe((data)=>{
        //   this.authors=data;
        // });
      })
      
    }
    else{
      // this.authorService.getAllAuthors().subscribe((data)=>{
      //   this.authors=data;
      // });
    }
  }
}
