import { Router } from '@angular/router';
import { OrderService } from './../../../services/order-service/order.service';
import { AuthService } from './../../../services/auth-service/auth.service';
import { GetOrder } from './../../../interfaces/order/get-order';
import { Component, OnInit } from '@angular/core';
import { StatusService } from 'src/app/services/status-service/status.service';
import { Status } from 'src/app/interfaces/status';

@Component({
  selector: 'app-orders-history',
  templateUrl: './orders-history.component.html',
  styleUrls: ['./orders-history.component.css']
})
export class OrdersHistoryComponent implements OnInit {

  orderBy:string='date';
  listOrder:string='descending';

  orders:GetOrder[]=[
    { id:1, customerFirstName:'Olha', customerLastName:'Melnyk', orderStatus:'In Progress',
    warehouseAddress: 'Kyiv', dealAddress: 'Kyiv', employeeAssignedFirstName: 'Valerii',
    employeeAssignedLastName: 'Ivanov', assignedTime:new Date, itemsIds: [1,2] },
    { id:1, customerFirstName:'Maria', customerLastName:'Adiy', orderStatus:'Ready',
    warehouseAddress: 'Kyiv 23', dealAddress: 'Kyiv', employeeAssignedFirstName: 'Darya',
    employeeAssignedLastName: 'Taras', assignedTime:new Date, itemsIds: [2,3] }
  ];

  roles:string[]=[];
  statusList:Status[]=[
    {name: 'Ready'},
    {name: 'In Progress'},
    {name: 'Done'},
    {name: 'Closed'}
  ];

  constructor(
    private orderService:OrderService,
    private authService:AuthService,
    private statusService:StatusService,
    private router:Router
  ) {
    this.orderService.getAllOrders().subscribe((data)=>{
      this.orders=data;
    });
    // this.statusService.getAllStatuses().subscribe((data)=>{
    //   this.statusList=data;
    // });
   }

  ngOnInit(): void {
    let currentUser=this.authService.getCurrentUserValue();
    if(currentUser!=null)
      this.roles=currentUser.roles;
  }

  onChanged(item:any){
    this.orderBy=item[0];
    this.listOrder=item[1];
  }

  onDetailesRequested(orderId:number){
    this.router.navigate(['/order',orderId]);
  }

  onDelete(orderId:number){
    
  }

  onSaveChanges(orderId:number){
    
  }
}
