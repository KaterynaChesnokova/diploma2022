import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-order-by-list',
  templateUrl: './order-by-list.component.html',
  styleUrls: ['./order-by-list.component.css']
})
export class OrderByListComponent implements OnInit {

  orderBySelected:string='creationDateDesc';
  orderBy:string='date';
  order:string='descending';

  @Output() onChanged=new EventEmitter();

  constructor() { }

  ngOnInit(): void {
  }

  onOrderChange(){
    if(this.orderBySelected=='creationDateDesc'){
       this.orderBy=this.orderBySelected;
       this.order='descending';
    }
    else if(this.orderBySelected=='creationDateAsc'){
      this.orderBy= 'date';
      this.order='ascending';
    }
    else if(this.orderBySelected=='customerEmail'){
      this.orderBy= 'email';
      this.order='descending';
    }

    this.onChanged.emit( [this.orderBy, this.order]);
  }
}
