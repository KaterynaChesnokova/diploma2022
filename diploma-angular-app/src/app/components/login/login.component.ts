import { ErrorResponse } from './../../interfaces/error-response';
import { Login } from '../../interfaces/authentication/login';
import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth-service/auth.service';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
})
export class LoginComponent implements OnInit {

  loginError!: string;
  loginForm!: FormGroup;

  constructor(private authService: AuthService, private router: Router) { }

  ngOnInit(): void {
    this.loginError = '';
    this.loginForm= new FormGroup({
      email: new FormControl(''),
      password: new FormControl(''),
    });
  }

  login(){
    let model:Login=this.loginForm.value;
    this.authService.login(model).subscribe(
      (user) => {
        this.router.navigate(['public']);
      },
      (exc:ErrorResponse) => {
        this.loginError = exc.Message;
      }
    );
  }
}
