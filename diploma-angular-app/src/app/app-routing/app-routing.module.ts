import { OrderCreateComponent } from './../components/order/order-create/order-create.component';
import { EmployeeViewComponent } from '../components/employee/employee-view/employee-view.component';
import { EmployeeCreateComponent } from '../components/employee/employee-create/employee-create.component';
import { PublicComponent } from './../components/public/public.component';
import { EmployeesComponent } from '../components/employee/employee/employee.component';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './../components/register/register.component';
import { LoginComponent } from './../components/login/login.component';
import { OrdersHistoryComponent } from '../components/order/orders-history/orders-history.component';
import { AuthGuard } from '../helpers/auth-guard';
import { UnauthorizedComponent } from '../components/unauthorized/unauthorized.component';

const routes: Routes = [
  {path: 'public', component: PublicComponent},
  {path: 'unauthorized', component: UnauthorizedComponent},

  {path: 'login', component: LoginComponent},
  {path: 'register', component: RegisterComponent},

  {
    path: 'employees', component: EmployeesComponent,
    canActivate: [AuthGuard],
    data: {
      role: 'Superadmin'
    }
  },
  {
    path: 'employee-create', component: EmployeeCreateComponent,
    canActivate: [AuthGuard],
    data: {
      role: 'Superadmin'
    }
  },
  {
    path: 'employee-view', component: EmployeeViewComponent,
    canActivate: [AuthGuard],
    data: {
      role: 'Superadmin'
    }
  },
  {
    path: 'orders-history', component: OrdersHistoryComponent,
    canActivate: [AuthGuard],
    data: {
      role: ['CustomerRelations', 'Inspector', 'Customer']
    }
  },
  {
    path: 'order-create', component: OrderCreateComponent,
    canActivate: [AuthGuard],
    data: {
      role: ['Customer']
    }
  },
  // {path: 'order', component: OrderComponent},



  { path: '', redirectTo: 'public', pathMatch: 'full' },
  { path: '**', redirectTo: 'public' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
