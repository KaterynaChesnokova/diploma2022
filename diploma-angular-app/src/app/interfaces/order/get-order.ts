export interface GetOrder{
    id:number,
    customerFirstName:string,
    customerLastName:string,
    orderStatus:string,
    warehouseAddress:string,
    dealAddress:string,
    employeeAssignedFirstName:string,
    employeeAssignedLastName:string,
    assignedTime:Date,
    itemsIds:number[]
}