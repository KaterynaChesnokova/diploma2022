export interface AddOrder{
    id:number,
    customerId:number,
    orderStatusId:number,
    dealAddressId:number,
    itemsIds:number[]
}