export interface Employee{
    id :string,
    email :string,
    first_name:string,
    last_name:string,
    passwordHash :string,
    isDeleted:boolean,
    officeId:number,
    ordersIds:number[],
    role:string
}