export interface AddEmployee{
    id :string,
    email :string,
    first_name:string,
    last_name:string,
    passwordHash :string,
    isDeleted:boolean,
    role:string
}