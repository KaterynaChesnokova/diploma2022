export interface ViewEmployee{
    id :string,
    email :string,
    first_name:string,
    last_name:string,
    isDeleted:boolean,
    role:string
}