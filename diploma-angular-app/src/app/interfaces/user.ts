export interface User {
    id :string,
    email :string,
    first_name:string,
    second_name:string,
    securityStamp:string
    passwordHash :string,
    phoneNumber :string,
    normalizedEmail :string,
    isDeleted:boolean
}
