export interface LoggedUser {
    id:string,
    email :string,
    token :string,
    expiration:Date,
    roles:string[]    
}
