import { AddOrder } from './../../interfaces/order/add-order';
import { GetOrder } from './../../interfaces/order/get-order';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  url='https://localhost:44301/api/Order/';

  constructor(private http: HttpClient) { }

  getOrder(id:number):Observable<GetOrder>{
    return this.http.get<GetOrder>(this.url+id);
  }

  getAllOrders():Observable<GetOrder[]>{
    return this.http.get<GetOrder[]>(this.url);
  }

  addOrder(newOrder:AddOrder):Observable<any>{
    return this.http.post(this.url,newOrder);
  }

  deleteOrder(id:number):Observable<any>{
    return this.http.delete(this.url+id);
  }
}
