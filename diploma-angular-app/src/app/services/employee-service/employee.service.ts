import { AddEmployee } from './../../interfaces/employee/add-employee';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Employee } from 'src/app/interfaces/employee/employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  url='https://localhost:44301/api/Employee/';

  constructor(private http: HttpClient) { }

  getEmployee(id:number):Observable<Employee>{
    return this.http.get<Employee>(this.url+id);
  }

  getAllEmployees():Observable<Employee[]>{
    return this.http.get<Employee[]>(this.url);
  }

  addEmployee(newEmployee:AddEmployee):Observable<any>{
    return this.http.post(this.url,newEmployee);
  }

  deleteEmployee(id:number):Observable<any>{
    return this.http.delete(this.url+id);
  }
}
