import { Role } from './../../interfaces/role';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { map } from "rxjs/operators";

@Injectable({
  providedIn: 'root'
})
export class RoleService {

  url='https://localhost:44325/api/Authenticate/';

  constructor(private http: HttpClient) { }

  getAllRoles():Observable<Role[]>{
    return this.http.get<Role[]>(this.url+'roles');
  }

  getEmployeeRoles():Observable<Role[]>{
    return this.http.get<Role[]>(this.url+'employee-roles');
  }
}
