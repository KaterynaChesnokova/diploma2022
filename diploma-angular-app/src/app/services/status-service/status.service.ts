import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { Status } from 'src/app/interfaces/status';

@Injectable({
  providedIn: 'root'
})
export class StatusService {

  url='https://localhost:44301/api/Status/';

  constructor(private http: HttpClient) { }

  getStatus(id:number):Observable<Status>{
    return this.http.get<Status>(this.url+id);
  }

  getAllStatuses():Observable<Status[]>{
    return this.http.get<Status[]>(this.url);
  }
}
